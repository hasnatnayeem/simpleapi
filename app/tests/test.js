const request = require('supertest')
const app = require('../app.js')

describe('Post Endpoints', () => {
  it('should fetch todos', async () => {
    const res = await request(app)
      .get('/todos')
    expect(res.statusCode).toEqual(200)
    expect(res.body).toHaveProperty('data')
  })
})