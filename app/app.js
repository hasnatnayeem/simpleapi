const express = require('express')
const app = express()
const todos = require('./data.json')

const HOSTNAME = process.env.HOSTNAME ? process.env.HOSTNAME : undefined

app.get('/todos', (req, res) => {
    res.json({
        hostname: HOSTNAME,
        message: 'All todo items',
        data: todos
    })
})

app.get('/todos/:id', (req, res) => {
    res.json({
        hostname: HOSTNAME,
        message: 'Single todo item',
        data: todos.filter(todo => todo.id == req.params.id)[0]
    })
})

module.exports = app